﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TplMultiThreadAppConsole
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var timer = Stopwatch.StartNew();
            const int size = 100;
            var random1 = new Random();
            var random2 = new Random(random1.Next());
            var a = Task.Factory.StartNew(() => random1.NextMatrix(size, size));
            var b = Task.Factory.StartNew(() => random2.NextMatrix(size, size));
            Console.WriteLine(a.Result.ToString("Матрица А: {0}x{1} (Sum = {2})"));
            Console.WriteLine(b.Result.ToString("Матрица B: {0}x{1} (Sum = {2})"));
            Console.WriteLine("Инициализация: " + timer.ElapsedMilliseconds + " ms");

            var c = MultMatrix(a.Result, b.Result);
            Console.WriteLine(c.ToString("Перемноженная матрица: {0}x{1} (Sum = {2})"));
            Console.WriteLine("Затраченное время: " + timer.ElapsedMilliseconds + " ms");
            if (args.Length == 0) Console.ReadKey();
        }

        public static int[,] NextMatrix(this Random rnd, int x, int y, int maxValue = 10)
        {
            var a = new int[x,y];
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    a[i, j] = rnd.Next(0, maxValue);
                }
            }
            return a;
        }
        
        public static int[,] MultMatrix (int[,] a, int[,] b)
        {
            var matrixSet = new MultMatrixHelper(a, b);
            // for (int i = 0, x = a.GetLength(0); i < x; i++) matrixSet.MultRow(i); /* 
            var options = new ParallelOptions {MaxDegreeOfParallelism = Environment.ProcessorCount};
            Parallel.For(0, a.GetLength(0), options, matrixSet.MultRow); /**/
            return matrixSet.Result;
        }

        public static string ToString(this int[,] a, string format)
        {
            long sum = 0;
            for (int i = 0, x = a.GetLength(0); i < x; i++)
            {
                for (int j = 0, y = a.GetLength(1); j < y; j++)
                {
                    sum += a[i, j];
                }
            }
            return string.Format(format, a.GetLength(0), a.GetLength(1), sum);
        }

        private class MultMatrixHelper
        {
            private readonly int[,] a;
            private readonly int[,] b;
            public readonly int[,] Result;

            public MultMatrixHelper(int[,] a, int[,] b)
            {
                this.a = a;
                this.b = b;
                Result = new int[a.GetLength(0), b.GetLength(1)];
            }

            public void MultRow(int i)
            {
                for (int j = 0, y = b.GetLength(1); j < y; j++)
                {
                    for (int k = 0, z = b.GetLength(0); k < z; k++)
                    {
                        Result[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
        }
    }
}
