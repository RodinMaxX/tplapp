﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MulttAppToFile
{
    public static class Program
    {
        static void Main(string[] args)
        {
            const string dirpath = @"D:\PROJECTS\TPLApp\TPLMultiThreadApp\MulttAppToFile\TEST";
            var timer = Stopwatch.StartNew();
            var files = Directory.GetFiles(dirpath);
            Console.WriteLine("Получение списка файлов: " + timer.ElapsedMilliseconds + " ms");
            using (var output = new StreamWriter("result.dat", false, Encoding.Default))
            {
                Console.WriteLine("Обработка (данные будут сохренены в отдельный файл)...");
                Parallel.ForEach(files, output.ProcessFile);
            }
            Console.WriteLine("Затраченное время: " + timer.ElapsedMilliseconds + " ms");
            if (args.Length == 0) Console.ReadKey();
        }

        public static void ProcessFile(this StreamWriter output, string filePath)
        {
            using (var fileStream = new StreamReader(filePath))
            {
                var file = Path.GetFileName(filePath);
                try
                {
                    var a = (fileStream.ReadLine() ?? "").Split(' ');
                    var x = double.Parse(a[1], CultureInfo.InvariantCulture);
                    var y = double.Parse(a[2], CultureInfo.InvariantCulture);
                    if (a[0] == "1")
                    {
                        output.WriteLineAsync("{0}: {1} * {2} = {3}", file, x.ToString(), y.ToString(), (x*y).ToString());
                    }
                    else if (a[0] == "2")
                    {
                        output.WriteLineAsync("{0}: {1} / {2} = {3}", file, x.ToString(), y.ToString(), (x/y).ToString());
                    }
                    else
                    {
                        output.WriteLineAsync(file + ": Unknown operation: " + a[0]);
                    }
                }
                catch (Exception ex)
                {
                    output.WriteLineAsync(file + ": ERROR: " + ex.Message);
                }
            }
        }

        public static void WriteLineAsync(this StreamWriter output, string format, params object[] args)
        {
            var s = string.Format(format, args);
            lock (output)
            {
                output.WriteLine(s);
            }
        }
    }
}
